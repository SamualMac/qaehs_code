function [ ref_c_spec ] = ref_c_spec_generator(EX_Mass,mz_values,mz_Int,MZ_PW,Int_MS1)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here

%%
%MS1
L_b=EX_Mass-(MZ_PW/2);
H_b=EX_Mass+(MZ_PW/2);

x=[L_b,EX_Mass-0.01,EX_Mass,EX_Mass+0.01,H_b];
y=[0,0.4*Int_MS1,Int_MS1,0.4*Int_MS1,0];

%%
%MS2
ms2=zeros(5,size(mz_values(mz_values>0),1));
Int2=ms2;

for i=1:size(mz_values(mz_values>0),1)
    tv1=mz_values(mz_values>0);
    tv2=tv1(i);
    ms2(1,i)=tv2-(MZ_PW/2);
    ms2(2,i)=tv2-0.01;
    ms2(3,i)=tv2;
    
    ms2(4,i)=tv2+0.01;
    ms2(5,i)=tv2+(MZ_PW/2);
    
    Int2(1,i)=0;
    Int2(2,i)=0.4*mz_Int(mz_values==tv2);
    Int2(3,i)=mz_Int(mz_values==tv2);
    Int2(4,i)=0.4*mz_Int(mz_values==tv2);
    Int2(5,i)=0;
    
    
    
    
    
end
    

MS2_v=reshape(ms2,[],1);
MS2_Int=reshape(Int2,[],1);

ref_c_spec.MS1_v=x;
ref_c_spec.MS1_int=y;
ref_c_spec.MS2_v=MS2_v;
ref_c_spec.MS2_int=MS2_Int;


% subplot(1,2,1)
% plot(ref_c_spec.MS1_v,ref_c_spec.MS1_int)
% hold on
% stem(EX_Mass,Int_MS1)
% hold off
% 
% xlabel('Mass')
% ylabel('Relative Intensity')
% 
% subplot(1,2,2)
% plot(ref_c_spec.MS2_v,ref_c_spec.MS2_int)
% hold on
% stem(mz_values,mz_Int)
% hold off
% xlabel('Mass')
% ylabel('Intensity')




end

