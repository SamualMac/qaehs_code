% Copyright (C) 2015 SSA
%
%This program is free software; you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.

% Author: SSA <SSA@FQVHV32-L>
%Created: 2015-11-24

function [total_intensity,t0,t_end,Mz_values,Mz_intensity,scan_duration] = netCDF_import (File_name)
%File_name='011113_34.CDF'; for test

%pkg load netcdf
%pkg load octcdf
%nc = netcdf(File_name,'r');
%%%%%%
%Import the variables
a_d_sampling_rate=ncread(File_name,'a_d_sampling_rate');
resolution=ncread(File_name,'resolution');
actual_scan_number=ncread(File_name,'actual_scan_number');
time_range_min=ncread(File_name,'time_range_min');
time_range_max=ncread(File_name,'time_range_max');
flag_count=ncread(File_name,'flag_count');


scan_acquisition_time=ncread(File_name,'scan_acquisition_time');% this parameter gives the beginning and the end of the program (s)
scan_duration=ncread(File_name,'scan_duration');
inter_scan_time=ncread(File_name,'inter_scan_time');
total_intensity=ncread(File_name,'total_intensity'); %The TIC
mass_range_min=ncread(File_name,'mass_range_min'); %min mass range
mass_range_max=ncread(File_name,'mass_range_max');  %max mass range
scan_index=ncread(File_name,'scan_index');
point_count=ncread(File_name,'point_count');
mass_values = ncread(File_name,'mass_values'); %m/z values
intensity_values=ncread(File_name,'intensity_values'); %intensity at each m/z value


%defining the starting point and the end point of the chromatogram
t0=scan_acquisition_time(1)/60; % min
t_end=scan_acquisition_time(end)/60; % min

%

Max_Mz_displacement=max(point_count);
Mz_values=zeros(length(scan_index),Max_Mz_displacement);
Mz_intensity=zeros(length(scan_index),Max_Mz_displacement);



for i=1:length(scan_index)-1
    Mz_values(i,1:point_count(i))=mass_values(scan_index(i)+1:scan_index(i+1)); % something here
    Mz_intensity(i,1:point_count(i))=intensity_values(scan_index(i)+1:scan_index(i+1));
end











end
