clc
clear 
close all
%%
%Project Folder
% Project_name='NMR';
% p=pwd();
% Foldername=strcat(p,'\',Project_name);
% 
% if (exist(Foldername,'dir')==0)
%     mkdir(Foldername)
%     
% end
% cd(Foldername)
%%
%Import the Chromatogram
% Low_Ene="../data/Low_Energy/VANN-FINLAND01.CDF"; % sjm change path.
% High_Ene="../data/High_Energy/VANN-FINLAND02.CDF";

% sjm INSERT START
path_low = '../data/Low_Energy';
path_high = '../data/High_Energy';
files_array_low = strcat(path_low, filesep, '*.CDF'); 
files_array_high = strcat(path_high, filesep, '*.CDF'); 
% targets contents of high and low energy batches in low and high, respectively
targets_low = ls(files_array_low); 
targets_high = ls(files_array_high); 
file_low = strcat(targets_low(1,:)); 
file_high = strcat(targets_low(1,:)); 
% !!! sjm INSERT END !!!

[total_intensity,t0,t_end,Mz_values,Mz_intensity,scan_duration] = ...
    netCDF_import (file_low);

[total_intensity2,t0_2,t_end_2,Mz_values_2,Mz_intensity_2, ...
    scan_duration_2] = netCDF_import (file_high);

data.total_intensity=total_intensity;
data.total_intensity2=total_intensity2;
data.t0=t0;
data.t0_2=t0_2;
data.t_end=t_end;
data.t_end_2=t_end_2;
data.Mz_values=Mz_values;
data.Mz_values_2=Mz_values_2;
data.Mz_intensity=Mz_intensity;
data.Mz_intensity_2=Mz_intensity_2;
data.scan_duartion=scan_duration;
data.scan_duartion_2=scan_duration_2;

%%
%Mass Calibration
% Cal_file='D:\Data\MATLAB\Library_search_fun\NMR_data\VANN-MIX03.CDF';
% [total_intensity_c,t0_c,t_end_c,Mz_values_c,Mz_intensity_c,scan_duration_c] = netCDF_import (Cal_file);
% 
% m_Mz=size(data.Mz_values,1);
% Mass_Lock=556.2771;       %the mass lock
% [ Calibrated_mzs_h,Calibrated_mzs_l ] = LC_MS_cal(Mass_Lock,Mz_values_c,...
%     Mz_intensity_c,m_Mz,data.Mz_values_2,data.Mz_values);
% 
% data.Mz_values=Calibrated_mzs_l;
% data.Mz_values_2=Calibrated_mzs_h;


data_c=data;

clear data
%%
%Import the peak list
List=readtable('../data/Targets_3.xlsx'); % sjm change path

%%

%Load the library and clean it

path_MB='../data/MassBank_matlab.mat';
source='ESI';
mode='POSITIVE';

load(path_MB,'data');

[ MassBank ] = comp_extractor( data,source,mode);

%%
%Deconvolution+Lib match

W=15;
min_int=800;
Mass_tol=0.01;
R_min=0.85;
P_max=0.05;
r_t=3;
ms_w=15;
n_p=13;
ms_p_w=0.05;

for i=1:size(List.ID,1)
    if (isnan(List.ID(i))==0)
        Scan_num=round(List.Retention_time(i));
        Ions=[round(List.MZ(i),3),100];
        
        [ spec ] = Frag_extractor_v2(data_c,Scan_num,W,min_int,Mass_tol,R_min,P_max,r_t,ms_w,Ions,ms_p_w);
    end
    
    %%
    %Library search and save the results
    if (isempty(spec.ms_v2)==0)
        disp(List.MZ(i))
        Parent=0;
        ms_tol_w=0.1; %Da
        Mass_tollerance=0.01;
        mz_precision=3;
        Weigth_fun=[1,1,1,1,1,1,1]; %[ion_match1,ion_match2,MZ_e1,MZ_e2,MZ_e3,D_match,R_match]
        mode='POSITIVE';
        path='../data/adducts/Pos_adducts.xlsx';
        [ Final_table ] = Lib_search(Parent,ms_tol_w,MassBank,spec,mz_precision,Mass_tollerance,Weigth_fun,mode,path);
        disp(Final_table)
%         writetable(Final_table,strcat('D:\Data\MATLAB\Library_search_fun\NMR\',num2str(List.ID(i)),'.txt'))
    end
    pause %%% wtf 
end












    
    
    
    