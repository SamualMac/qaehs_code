import DeconvLibrarySearch_v1
from os.path import join

# this 'data_path' will depend on where you save data
data_path = join('~','data','qaehs','sim','data')

# elements in 'd_set' list corresponds to different chemical parameter
d_set = [15.0, 800.0, 0.01, 0.85, 0.05, 3.0, 15.0, 13.0, 0.05, 0.01, 3.0]

# path to target list (what we're looking for)
target_path = join(data_path, "Targets_2.xlsx")

path_low = join(data_path, "Low_Energy")
path_high = join(data_path, "High_Energy")

path_MB = join(data_path, 'MassBank_matlab.mat')
source = 'ESI'
mode = 'POSITIVE'
path_adducts = join(data_path, 'adducts', 'Pos_adducts.xlsx')

output_ulsa = join(data_path, 'ULSA')
output_deconv = join(data_path, 'deconv')

# start up the matlab runtime engine
dl = DeconvLibrarySearch_v1.initialize()

dl.DeconvLibrarySearch_v1(d_set, target_path, path_low, path_high,
    path_MB, source, mode, path_adducts, output_ulsa, 
                          output_deconv, nargout=0)

# never forget to terminate!
dl.terminate()