%% What this test is for:
% -- Library CASE: LibrarySearch
% -- for library search alone
% - [0] Testing procedure to implement ImportDeconv dependent functions
%% USER DEFINED SETTINGS - would be in python

clear
% ensure wd correct
cd ~/Repositories/qaehs_code/matlab_functions/

% SET LIBRARY_SEARCH CONSTANTS (i.e user inputs)
path_MB = '~/Repositories/qaehs/sim/data/MassBank_matlab.mat';
source = 'ESI';
mode = 'POSITIVE';
path_adducts = '~/Repositories/qaehs/sim/data/adducts/Pos_adducts.xlsx';

% Constants to read deconv_xxx.txt data
path_to_spec = '~/Repositories/qaehs/sim/data/deconv';
output_ulsa = '~/Repositories/qaehs/sim/data/ULSA';

%% FOLLOWING CODE WOULD BE COMPILED
tic
LibrarySearch_v1(path_MB, source, mode, path_adducts, path_to_spec, ...
    output_ulsa)
toc