import ImportDeconv_v1

# elements in 'd_set' list corresponds to different chemical parameter
d_set = [15.0, 800.0, 0.01, 0.85, 0.05, 3.0, 15.0, 13.0, 0.05, 0.01, 3.0]
# path to target list (what we're looking for)
target_path = '~/data/qaehs/sim/data/Targets_1.xlsx'

# path to REPO not file.
path_low = '~/data/qaehs/sim/data/Low_Energy'
path_high = '~/data/qaehs/sim/data/High_Energy'

# start up the matlab runtime engine
d = ImportDeconv_v1.initialize()

# this is how you run the script
d.ImportDeconv_v1(d_set, target_path, path_low, path_high, nargout=0)

# dont forget to terminate the runtime engine at end of run!
d.terminate()