%% What this test is for: 
% - [1] Testing procedure to implement ImportDeconv dependent functions
% - [1] Test if able to save to .txt file.
% - [0] Test if output data is correct
% - [0] Test of able to do nested loop - targets THEN files.

%% USER DEFINED SETTINGS - would be in python.
% @ensure: file_low, and file_high variables are path to and including the
% .CDF file being imported.
% ----------------------------------
clear

% change working directory to updated matlab configureation
cd ~/Repositories/qaehs_code/matlab_functions/

% SET DECONVOLUTION SETTINGS
W=15;
min_int=800;
Mass_tol=0.01;
R_min=0.85;
P_max=0.05;
r_t=3;
ms_w=15;
%n_p=13; % !!! NOTE THAT THIS IS NOT USED
ms_p_w=0.05;
Mass_w=0.01;
S2N=3;

d_set = [W min_int Mass_tol R_min P_max r_t ms_w ms_p_w Mass_w S2N];

% SET PATH TO TARGETS
target_path = '~/Repositories/qaehs/sim/data/Targets_3.xlsx'; 

% SET PATH TO DATA
path_low = '~/Repositories/qaehs/sim/data/Low_Energy';
path_high = '~/Repositories/qaehs/sim/data/High_Energy';

output_deconv = '~/Repositories/qaehs/sim/data/deconv';

%% FOLLOWING CODE WOULD BE COMPILED
tic
ImportDeconv_v1(d_set, target_path, path_low, path_high, output_deconv)
toc