import LibrarySearch_v1
from os.path import join

# this 'data_path' will depend on where you save data
data_path = join('~','data','qaehs','sim','data')

path_MB = join(data_path, 'MassBank_matlab.mat')
source = 'ESI'
mode = 'POSITIVE'
path_adducts = join(data_path, 'adducts', 'Pos_adducts.xlsx')
path_to_spec = join(data_path, 'deconv')
output_ulsa = join(data_path, 'ULSA')

# start up the matlab runtime engine
l = LibrarySearch_v1.initialize()

# this is how you run the script
l.LibrarySearch_v1(path_MB, source, mode, path_adducts, 
                   path_to_spec, output_ulsa, nargout=0)

# never forget to terminate!!!
l.terminate()