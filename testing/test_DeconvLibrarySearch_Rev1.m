%% What this test is for: 
% -- GLOBAL CASE: DeconvLibrarySearch
% -- for deconvolution and library search together
% - [0] Just have it work as per diagram
%
%% USER DEFINED SETTINGS - would be in python.
% @ensure: file_low, and file_high variables are path to and including the
% .CDF file being imported.
% ----------------------------------
clear
% note that the script to line 23 would be implemented by python in
cd ~/Repositories/qaehs_code/matlab_functions/

% SET DECONVOLUTION SETTINGS
W=15;
min_int=800;
Mass_tol=0.01;
R_min=0.85;
P_max=0.05;
r_t=3;
ms_w=15;
%n_p=13; % !!! NOTE THAT THIS IS NOT USED
ms_p_w=0.05;
Mass_w=0.01;
S2N=3;

d_set = [W min_int Mass_tol R_min P_max r_t ms_w ms_p_w Mass_w S2N];

% SET PATH TO TARGETS
target_path = '~/Repositories/qaehs/sim/data/Targets_3.xlsx';

% SET PATH TO DATA
path_low = '~/Repositories/qaehs/sim/data/Low_Energy';
path_high = '~/Repositories/qaehs/sim/data/High_Energy';

% SET LIBRARY_SEARCH CONSTANTS (i.e user inputs)
path_MB = '~/Repositories/qaehs/sim/data/MassBank_matlab.mat';
source = 'ESI';
mode = 'POSITIVE';
path_adducts = '~/Repositories/qaehs/sim/data/adducts/Pos_adducts.xlsx';

output_ulsa = '~/Repositories/qaehs/sim/data/ULSA';
output_deconv = '~/Repositories/qaehs/sim/data/deconv';

%% test
tic

DeconvLibrarySearch_v1(d_set, target_path, path_low, path_high,...
    path_MB, source, mode, path_adducts, output_ulsa, output_deconv)

toc 