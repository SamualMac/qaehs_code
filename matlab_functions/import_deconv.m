% Collaboration between QAEHS & NIVA
% Author: Saer Samanipour
% Assisted by: Samual MacDonald
% 
% @file_low, @file_high <string>: paths to files.
%
% @target <table>: an individual row instance of a target compound from the 
% "targets_n.xlsx" table.
% 
% @deconv_settings <list / vector>: user specifified settings for deconv,
% list([W, min_int, Mass_tol, R_min, P_max, r_t, ms_w, ms_p_w])
% NB: order matters for deconv_settings.

function user_spectra = import_deconv(HRMS_data, target, ...
                                      scan_num, ions, deconv_settings)
    % unpack deconv_settings
    W = deconv_settings(1);
    min_int = deconv_settings(2);
    Mass_tol = deconv_settings(3);
    R_min = deconv_settings(4);
    P_max = deconv_settings(5);
    r_t = deconv_settings(6);
    ms_w = deconv_settings(7);
    ms_p_w = deconv_settings(8);
    Mass_w = deconv_settings(9);
    S2N = deconv_settings(10);
    
    % Communicate to user
    disp("Low and high energy LC-HRMS data successfully imported")
    disp("Commencing deconvolution for Target ID: "+target.ID)
    disp("Spectra details: ");
    % return spectra...
    user_spectra = Frag_extractor_v1(HRMS_data, scan_num, W, min_int, ...
                                      Mass_tol, R_min, P_max, r_t, ms_w, ...
                                      ions, ms_p_w, Mass_w, S2N);
    % if exist .txt
    % append
    % else 
    % write with header.
    disp("Deconvolution complete for target ID "+target.ID);
     
     
end