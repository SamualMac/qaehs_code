tic 
a = MassBank.Access;
b = MassBank.Exact_mass;
c = MassBank.MS_Type;
d = MassBank.mz_Int;
e = MassBank.mz_rel_Int;
f = MassBank.mz_values;
g = MassBank.Name;
test = {a b c d e f g};
toc
so = "Time to unpack and pack struct in matrix form. "+...
    "~60 sec for jsondecoder...."
clear a 
clear b
clear c
clear d
clear e
clear f
clear g
clear test