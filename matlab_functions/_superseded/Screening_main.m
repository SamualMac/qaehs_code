clc
clear 
close all
%%
%Project Folder
Project_name='NMR';
p=pwd();
Foldername=strcat(p,'\',Project_name);

if (exist(Foldername,'dir')==0)
    mkdir(Foldername)
    
end
cd(Foldername)
%%
%Import the Chromatogram
Low_Ene='D:\Data\MATLAB\Library_search_fun\NMR_data\VANN-FINLAND01.CDF';
High_Ene='D:\Data\MATLAB\Library_search_fun\NMR_data\VANN-FINLAND02.CDF';
[total_intensity,t0,t_end,Mz_values,Mz_intensity,scan_duration] = netCDF_import (Low_Ene);
[total_intensity2,t0_2,t_end_2,Mz_values_2,Mz_intensity_2,scan_duration_2] = netCDF_import (High_Ene);
data.total_intensity=total_intensity;
data.total_intensity2=total_intensity2;
data.t0=t0;
data.t0_2=t0_2;
data.t_end=t_end;
data.t_end_2=t_end_2;
data.Mz_values=Mz_values;
data.Mz_values_2=Mz_values_2;
data.Mz_intensity=Mz_intensity;
data.Mz_intensity_2=Mz_intensity_2;
data.scan_duartion=scan_duration;
data.scan_duartion_2=scan_duration_2;

%%
%Mass Calibration
% Cal_file='D:\Data\MATLAB\Library_search_fun\NMR_data\VANN-MIX03.CDF';
% [total_intensity_c,t0_c,t_end_c,Mz_values_c,Mz_intensity_c,scan_duration_c] = netCDF_import (Cal_file);
% 
% m_Mz=size(data.Mz_values,1);
% Mass_Lock=556.2771;       %the mass lock
% [ Calibrated_mzs_h,Calibrated_mzs_l ] = LC_MS_cal(Mass_Lock,Mz_values_c,...
%     Mz_intensity_c,m_Mz,data.Mz_values_2,data.Mz_values);
% 
% data.Mz_values=Calibrated_mzs_l;
% data.Mz_values_2=Calibrated_mzs_h;


data_c=data;
clear data
%%
%Import the peak list
List=readtable('D:\Data\MATLAB\Library_search_fun\NMR_data\Targets_1.xlsx');

%%

% Load the library and clean it

path_MB='D:\Data\MATLAB\MassBank_matlab.mat';
source='ESI'; % ionisation source - i.e. method category (reduce) % sjm 180228
mode='POSITIVE'; % polarity of ion source % sjm 180228

% load(path_MB,'data'); % sjm 180228

[ MassBank ] = comp_extractor_SSA(path_MB,source,mode); % compile MassBank for smaller dataset % sjm 180228

%%
%Deconvolution+Lib match

W=15;
min_int=800;
Mass_tol=0.01;
R_min=0.85;
P_max=0.05;
r_t=3;
ms_w=15;
n_p=13;
ms_p_w=0.05;

for i=1:size(List.ID,1)
    if (isnan(List.ID(i))==0)
        Scan_num=round(List.Retention_time(i));
        Ions=[round(List.MZ(i),3),100];
        
        [ spec ] = Frag_extractor(data_c,Scan_num,W,min_int,Mass_tol,R_min,P_max,r_t,ms_w,Ions,ms_p_w);
    end
    
    %%
    %Library search and save the results
    if (isempty(spec.ms_v2)==0)
        disp(List.MZ(i))
        Parent=0;
        ms_tol_w=0.1; %Da
        Mass_tollerance=0.01;
        mz_precision=3;
        Weigth_fun=[1,1,1,1,1,1,1]; %[ion_match1,ion_match2,MZ_e1,MZ_e2,MZ_e3,D_match,R_match]
        mode='POSITIVE';
        path='D:\Data\MATLAB\PCA\Pos_adducts.xlsx';
        [ Final_table ] = Lib_search(Parent,ms_tol_w,MassBank,spec,mz_precision,Mass_tollerance,Weigth_fun,mode,path);
        disp(Final_table)
        writetable(Final_table, strcat('D:\Data\MATLAB\Library_search_fun\NMR\',num2str(List.ID(i)),'.txt'))
        
        
    end
    pause
end