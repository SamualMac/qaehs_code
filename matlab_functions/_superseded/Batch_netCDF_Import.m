function  Batch_netCDF_Import(path,path1)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

FileNames=strcat(path,filesep,'*.CDF');
FileNames1=strcat(path1,filesep,'*.CDF');

% list contents of high and low energy batches in a and a1, respectively
a=ls(FileNames); 
a1=ls(FileNames1); 

p=pwd();
Foldername=strcat(p,filesep,'Imported_files'); 

if (exist(Foldername,'dir')==0) % folder to ouput data to 
    mkdir(Foldername)
end

% k iterator marks index of file in FileNames, which has length a
for k=1:size(a,1) 
    
    File=strcat(a(k,:)); % low energy path % sjm edit 180227: removed "path, filesep"
    File1=strcat(a1(k,:)); % sjm edit 180227: removed "path1, filesep"
    [~,name,~] = fileparts(File); % take name of low energy
    [total_intensity,t0,t_end,Mz_values,Mz_intensity,scan_duration] = netCDF_import (File);
    [total_intensity2,t0_2,t_end_2,Mz_values_2,Mz_intensity_2,scan_duration_2] = netCDF_import (File1);
    data.total_intensity=total_intensity;
    data.total_intensity2=total_intensity2;
    data.t0=t0;
    data.t0_2=t0_2;
    data.t_end=t_end;
    data.t_end_2=t_end_2;
    data.Mz_values=Mz_values;
    data.Mz_values_2=Mz_values_2;
    data.Mz_intensity=Mz_intensity;
    data.Mz_intensity_2=Mz_intensity_2;
    data.scan_duartion=scan_duration;
    data.scan_duartion_2=scan_duration_2;
    save(strcat(Foldername,filesep,name,'.mat'),'data','-v7.3')
    disp(strcat('The file ',name,' has been successfully imported'))
   
    
end



end

