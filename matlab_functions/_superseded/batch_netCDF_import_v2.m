% Collaboration between QAEHS & NIVA
% Author: Saer Samanipour
% Assisted by: Samual MacDonald
% 
% @low <"string">: path to file
% @high <"string">: path to file
%
% returns <struct> object type

function  [ data ] = batch_netCDF_import_v2(file_low, file_high)

% call netCDF_import
[total_intensity, t0, t_end, Mz_values, Mz_intensity, ...
    scan_duration] = netCDF_import(file_low);

[total_intensity2, t0_2, t_end_2, Mz_values_2, Mz_intensity_2, ...
    scan_duration_2] = netCDF_import(file_high);

% create struct object
data.total_intensity=total_intensity;
data.total_intensity2=total_intensity2;
data.t0=t0;
data.t0_2=t0_2;
data.t_end=t_end;
data.t_end_2=t_end_2;
data.Mz_values=Mz_values;
data.Mz_values_2=Mz_values_2;
data.Mz_intensity=Mz_intensity;
data.Mz_intensity_2=Mz_intensity_2;
data.scan_duartion=scan_duration;
data.scan_duartion_2=scan_duration_2;

end

