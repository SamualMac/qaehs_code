function LibrarySearch(path_MB, source, mode, path_adducts, path_to_spec)
% ------------------------------------------------------------------------
% @ Functionality:
% ------------------
% Execute the ULSA algorithm as described in Samanipour et al. (2018) to 
% attain list of candidate compounds, sorted by target mass first and 
% sorted by score second.
% 
% NOTE: max_num_files is assumed under intitial limmitting conditions of 
%       this software. That is, there can be only a maximum of 5 files and 
%       50 targets for every test.
% 
% ------------------------------------------------------------------------
% @ Arguments:
% ------------------
% path_MB <string> :        Relative path to MassBank Library,
%                               e.g.'../data/MassBank_matlab.mat'
%
% source <string> :         (refer to Samanipour et al. (2018)), e.g. 'ESI'
%
% mode <string> :           Whether HRMS tested for positive or negative 
%                           ions (refer to Samanipour et al. (2018)), 
%                               e.g.'POSITIVE';
%
% path_adducts <string> :   Path to excel file listing candidate adducts, 
%                               e.g. '../data/adducts/Pos_adducts.xlsx'
%
% path_to_spec <string> :   Path to file containing compound MS spectra, 
%                               e.g. '../data/deconv'
% 
% ------------------------------------------------------------------------
% @ Saved:
% ------------------
% ULSA results saved to "ULSA_xxxxxxxxxxxx.csv", where "xxx...x" details 
% the UNIX time when file is saved.
% 
% ------------------------------------------------------------------------
% (c) Samanipour et al. (2018) 

%% TO COMPILE

% initiate variables
test_ID = 0;
% numFilePairs = 1;
currentLine = 1;
eof = false;
max_num_files = 5;
max_num_targets = 50;
max_num_results = 10*max_num_files*max_num_targets;
beg_ = 1;

if ~isempty(dir('../data/ULSA/ULSA_*.csv'))
    % Move previous file to _superseded doc.
    disp("Warning: Too many ULSA_*.csv files");
    movefile('../data/ULSA/ULSA_*', ...
        '../data/ULSA/_superseded/');
    disp("Older files in '~/data/ULSA/' have been moved to" + ...
        " '~/data/ULSA/_superseded/'.")
end
tic
while not(eof)
    clear s_ULSA % ensure we don't duplicate results from last iteration
    
    test_ID = test_ID + 1;
    disp("---------------------------------------------------------------")
    disp("Loading batch...")
    
    % read single batch of spectra data
    [user_spectra, meta, eof] = readSpecData(path_to_spec,currentLine);
    
    % extract meta data
    meta_split = strsplit(meta, ';');
    name_low = meta_split{1}(6:end); % "6:end" exclude labels within string
    name_high = meta_split{2}(8:end);
    target_id = meta_split{3}(13:end);
    target_mz = meta_split{4}(13:end);
    retention_time = meta_split{5}(20:end);
    
    disp("Target {ID: "+target_id+", MZ: "+target_mz+...
        ", Ret. Time: "+retention_time)
    
    currentLine = currentLine + 5; % for next iteration
        
    % if there are ms2 contents, extract single instance s_ULSA
    if ~isempty(user_spectra.ms_v2)
        [ s_ULSA ] = library_search(path_MB, path_adducts, source, ...
                                 mode, user_spectra);
    else
        disp("No mass spec. patner (MS2 data)")
    end
    
    if ~isempty(s_ULSA)
        [n_row_add, ~] = size(s_ULSA);
        disp("Positive target match in library. Saving results.")
    else
        n_row_add = 0;
        disp("Negative target match in library; possibly not registered.")
    end

    % write to ULSA_out
    if n_row_add ~= 0
        end_ = beg_ + n_row_add - 1;
        
            % instantiate empty table on first iteration
        if beg_ == 1
%             ULSA_out = cell2table(cell(max_num_results,16), ...
            ULSA_out = cell2table(cell(0, 16), ...
                'VariableNames', ...
                {'test_ID', 'low_file', 'high_file', 'target_id', ...
                'target_mz', 'retention_time', 'nr', 'Name', ...
                'Ref_ion_matched', 'User_ion_matched', ...
                'Parent_ion_error', 'Average_frag_error', ...
                'Std_frag_error', 'Direct_match', 'Reverse_match', ...
                'Score'});
        end
        
        for toAdd_index = 1:n_row_add
            % concat method
            new_row = {...
                test_ID, name_low, name_high, target_id, target_mz, ...
                retention_time, ...
                s_ULSA.nr(toAdd_index), ...
                s_ULSA.Name(toAdd_index), ...
                s_ULSA.Ref_ion_matched(toAdd_index), ...
                s_ULSA.User_ion_matched(toAdd_index), ...
                s_ULSA.Parent_ion_error(toAdd_index), ...
                s_ULSA.Average_frag_error(toAdd_index), ...
                s_ULSA.Std_frag_error(toAdd_index), ...
                s_ULSA.Direct_match(toAdd_index), ...
                s_ULSA.Reverse_match(toAdd_index), ...
                s_ULSA.Score(toAdd_index)...
            };
            ULSA_out = [ULSA_out; new_row];
        end

%         ULSA_out.test_ID(beg_:end_) = {str2double(test_ID)};
%         ULSA_out.low_file(beg_:end_) = {name_low};
%         ULSA_out.high_file(beg_:end_) = {name_high};
%         ULSA_out.target_id(beg_:end_) = {str2double(target_id)};
%         ULSA_out.target_mz(beg_:end_) = {str2double(target_mz)};
%         
%         ULSA_out.retention_time(beg_:end_) = {str2double(retention_time)};
%         ULSA_out.nr(beg_:end_) = num2cell(s_ULSA.nr);
%         ULSA_out.Name(beg_:end_) = num2cell(s_ULSA.Name);
%         ULSA_out.Ref_ion_matched(beg_:end_) = ...
%             num2cell(s_ULSA.Ref_ion_matched);
%         ULSA_out.User_ion_matched(beg_:end_) = ...
%             num2cell(s_ULSA.User_ion_matched);
%         ULSA_out.Parent_ion_error(beg_:end_) = ...
%             num2cell(s_ULSA.Parent_ion_error);
%         ULSA_out.Average_frag_error(beg_:end_) = ...
%             num2cell(s_ULSA.Average_frag_error);
%         ULSA_out.Std_frag_error(beg_:end_) = ...
%             num2cell(s_ULSA.Std_frag_error);
%         ULSA_out.Direct_match(beg_:end_) = num2cell(s_ULSA.Direct_match);
%         ULSA_out.Reverse_match(beg_:end_) = num2cell(s_ULSA.Reverse_match);
%         ULSA_out.Score(beg_:end_) = num2cell(s_ULSA.Score);
%         
%         beg_ = end_ + 1;
        
    end
    
end
disp("Time taken for concatenation method")
disp(toc)
disp("-----")
if eof % save to hard drive
%     ULSA_out = sortrows(ULSA_out, [5 17]);
    unix_time = round(posixtime(datetime),2);
    unix_time = num2str(unix_time);
    file_name = sprintf('../data/ULSA/ULSA_%014s.csv', unix_time);
    writetable(ULSA_out, file_name)
    disp("Saved results to /data/ULSA/ULSA_00"+num2str(unix_time)+".csv.")
end 

end