% this function is superseded

% deconvultion function takes HRMS data, target list, and user specified
% parameters.
% deconvolution was made in addition to "Frag_extractor_v2()" 



function spectra = deconvolution_ss(HRMS_path, target_path, W, min_int, ...
                                  Mass_tol, R_min, P_max, r_t, ms_w, ...
                                  ms_p_w)
    
    % HRMS_path : file path to .mat output from Batch_netCDF_Import.m 
    % target_path : file path to Targets_1.xlsx table
    % all other parameters are user specified
    
    data_c = load(HRMS_path);
    % load Scan_num
    List=readtable(target_path);

    for i=1:size(List.ID,1)
        if (isnan(List.ID(i))==0)
            Scan_num=round(List.Retention_time(i));
            Ions=[round(List.MZ(i),3), 100]; % say whaaa.
        
            spectra = Frag_extractor_v2(data_c, Scan_num, W, min_int, ...
                                  Mass_tol, R_min, P_max, r_t, ms_w, ...
                                  Ions, ms_p_w);
        end
                              
    end
    
end