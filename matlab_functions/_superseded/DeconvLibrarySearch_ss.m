function DeconvLibrarySearch(inputArg1,inputArg2)
% @ Functionality:
% ------------------
% Import HRMS data; execute the deconvolution algorithm (Samanipour et al.,
% 2018); output user spectra data to .txt; output user spectra to object
% that is passed into ULSA; execute ULSA (Samanipour et al., 2018) to 
% attain list of candidate compounds; output ULSA results to .csv.
% 
% NOTE: max_num_files is assumed under intitial limmitting conditions of 
%       this software. That is, there can be only a maximum of 5 files and 
%       50 targets for every test.
% 
% ------------------------------------------------------------------------
% @ Arguments:
% ------------------
% path_MB <string> :        Relative path to MassBank Library,
%                               e.g.'../data/MassBank_matlab.mat'
%
% source <string> :         (refer to Samanipour et al. (2018)), e.g. 'ESI'
%
% mode <string> :           Whether HRMS tested for positive or negative 
%                           ions (refer to Samanipour et al. (2018)), 
%                               e.g.'POSITIVE';
%
% path_adducts <string> :   Path to excel file listing candidate adducts, 
%                               e.g. '../data/adducts/Pos_adducts.xlsx'
%
% path_to_spec <string> :   Path to file containing compound MS spectra, 
%                               e.g. '../data/deconv'
% 
% ------------------------------------------------------------------------
% @ Saved:
% ------------------
% Data saved to "deconv_xxxxxxxxxxxxxx.txt", where "xxx...xx" details the 
% UNIX time when deconvolution spectra is saved.
% 
% ULSA results saved to "ULSA_xxxxxxxxxxxx.csv", where "xxx...x" details 
% the UNIX time when file is saved.
% 
% ------------------------------------------------------------------------
% (c) Samanipour et al. (2018) 
outputArg1 = inputArg1;
outputArg2 = inputArg2;
end

