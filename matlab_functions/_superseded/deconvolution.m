% Collaboration between QAEHS & NIVA
% Author: Saer Samanipour
% Assisted by: Samual MacDonald
% 
% @data_c <struct>:
% 
% @target <table>: an individual row instance of a target compound from the 
% "targets_n.xlsx" table.
% 
% @deconv_settings <list / vector>: user specifified settings for deconv,
% list([W, min_int, Mass_tol, R_min, P_max, r_t, ms_w, ms_p_w])
% NB: order matters for deconv_settings.
% 
function [ spec ] = deconvolution(data_c, target, deconv_settings)
    %% 
    % unpack deconv settings                          
    W = deconv_settings(1);
    min_int = deconv_settings(2);
    Mass_tol = deconv_settings(3);
    R_min = deconv_settings(4);
    P_max = deconv_settings(5);
    r_t = deconv_settings(6);
    ms_w=deconv_settings(7);
    ms_p_w=deconv_settings(8);
    
    % set parameters for iteration
    Scan_num=round(target.Retention_time);
    Ions=[round(target.MZ,3),100];
    
    if (Scan_num-W > 1 && Scan_num+W < size(data_c.Mz_values,1))
        ms_v1=data_c.Mz_values(Scan_num-W:Scan_num+W, :);
        ms_in1=data_c.Mz_intensity(Scan_num-W:Scan_num+W, :);
        ms_v2=data_c.Mz_values_2(Scan_num-W:Scan_num+W, :);
        ms_in2=data_c.Mz_intensity_2(Scan_num-W:Scan_num+W, :);

    elseif (Scan_num-W <= 1&&Scan_num+W < size(data_c.Mz_values,1))
        ms_v1=data_c.Mz_values(1:Scan_num+W,:);
        ms_in1=data_c.Mz_intensity(1:Scan_num+W,:);
        ms_v2=data_c.Mz_values_2(1:Scan_num+W,:);
        ms_in2=data_c.Mz_intensity_2(1:Scan_num+W,:);

    elseif (Scan_num-W > 1 && Scan_num+W >= size(data_c.Mz_values,1))
        ms_v1=data_c.Mz_values(Scan_num-W:size(data_c.Mz_values,1),:);
        ms_in1=data_c.Mz_intensity(Scan_num-W:size(data_c.Mz_values,1),:);
        ms_v2=data_c.Mz_values_2(Scan_num-W:size(data_c.Mz_values_2,1),:);
        ms_in2=data_c.Mz_intensity_2(Scan_num-W:size(data_c.Mz_values_2,1),:);
    end

    %%
    %main ion extraction
    tv1=abs(Ions(1)-ms_v1);
    ref_spec=zeros(size(ms_v1,1),1);
    for j=1:size(ms_v1,1)
        [V,J]=min(tv1(j,:));
        if (V<=Mass_tol)
            ref_spec(j)=ms_in1(j,J);
        end
        clear V J    
    end

    %%
    %Low energy channel

    tv2=(find(ms_in1(W+1,:)>=min_int));
    MS_IN1=zeros(1,size(ms_in1,2));

    for j=1:size(tv2,2)
        tv3=ms_in1(W+1,tv2);
        tv4=find(ms_in1(W+1,:)==tv3(j),1);
        tv5=abs(ms_v1(W+1,tv4)-ms_v1);
        tv6=zeros(size(ms_v1,1),1);
        for k=1:size(ms_v1,1)
            [V,J]=min(tv5(k,:));
            if (V<=Mass_tol)
                tv6(k)=ms_in1(k,J);
            end
            clear V J
        end

        [M_v,M_i]=max(tv6);
        if (abs(M_i-W+1)<=r_t&&M_v>=min_int)
            E_p=W+find(ref_spec(W+1:end)<=0.1*ref_spec(W),1);
            I_p=(W+1)-find(flipud(ref_spec(1:W))<=0.1*ref_spec(W),1);


             [R,P] = corrcoef(tv6(I_p:E_p),ref_spec(I_p:E_p));

            if (R(2,1)>=R_min&&P(2,1)<=P_max)

               MS_IN1(ms_v1(W+1,:)==ms_v1(W+1,tv4))=ms_in1(W+1,ms_v1(W+1,:)==ms_v1(W+1,tv4));
            end
        end
    end

    %%
    %High energy channel
    Tv2=(find(ms_in2(W+1,:)>=min_int));
    MS_IN2=zeros(1,size(ms_in2,2));

    for j=1:size(Tv2,2)
        Tv3=ms_in2(W+1,Tv2);
        Tv4=find(ms_in2(W+1,:)==Tv3(j),1);
        Tv5=abs(ms_v2(W+1,Tv4)-ms_v2);
        Tv6=zeros(size(ms_v2,1),1);
        for k=1:size(ms_v2,1)
            [V,J]=min(Tv5(k,:));
            if (V<=Mass_tol)
                Tv6(k)=ms_in2(k,J);
            end
            clear V J
        end

        [M_v,M_i]=max(Tv6);
        if (abs(M_i-W+1)<=r_t&&M_v>=min_int) % always fails !!!
            E_p=W+find(ref_spec(W+1:end)<=0.1*ref_spec(W),1);
            I_p=(W+1)-find(flipud(ref_spec(1:W))<=0.1*ref_spec(W),1);

             [R,P] = corrcoef(Tv6(I_p:E_p),ref_spec(I_p:E_p));

            if (R(2,1)>=R_min&&P(2,1)<=P_max)

               MS_IN2(ms_v2(W+1,:)==ms_v2(W+1,Tv4))=ms_in2(W+1,ms_v2(W+1,:)==ms_v2(W+1,Tv4));
            end
        end
    end
    %%
    %Centeroiding the spectra
    [ ms_int1 ] = centroiding_MS_data( MS_IN1,min_int,ms_w );

    [ ms_int2 ] = centroiding_MS_data( MS_IN2,min_int,ms_w);


    %%
    %mass alignment

    for j=1:size(ms_v1,2)

        [tv3,tv4]=min(abs(ms_v1(j)-ms_v2(W+1,:)));
        if (ms_int2(tv4)<=0&&tv4>W&&tv4+W<=size(ms_int2,2))
            tv5=find(ms_int2(tv4-W:tv4+W)>=min_int);
            for k=1:size(tv5,2)
                tv6=tv4-W+tv5(k)-1;
                if (isempty(tv6)==0&&abs(tv6-tv4)<=W&&abs(ms_v2(W+1,tv4)-ms_v2(W+1,tv6))<=ms_p_w)
                    ms_int2(tv4)=ms_int2(tv6);
                    ms_int2(tv6)=0;
                end
            end

        elseif (ms_int2(tv4)<=0&&tv4>W&&tv4+W>size(ms_int2,2))
            tv5=find(ms_int2(tv4-W:end)>=min_int);
            for k=1:size(tv5,2)
                tv6=tv4-W+tv5(k)-1;
                if (isempty(tv6)==0&&abs(tv6-tv4)<=W&&abs(ms_v2(W+1,tv4)-ms_v2(W+1,tv6))<=ms_p_w)
                    ms_int2(tv4)=ms_int2(tv6);
                    ms_int2(tv6)=0;
                end
            end
        end
        
        clear tv2 tv3 tv4 tv5 tv6
        
    end

    spec.ms_v1=ms_v1;
    spec.ms_v2=ms_v2(W+1,ms_int2>0);
    spec.ms_in1=MS_IN1;
    spec.ms_in2=ms_int2(ms_int2>0);

    disp(spec)
    
end

