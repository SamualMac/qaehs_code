function ImportDeconv(d_set, target_path, path_low, path_high)
% ------------------------------------------------------------------------
% @ Functionality:
% ------------------
% Import HRMS data; execute the deconvolution algorithm (Samanipour et al.,
% 2018); output user spectra data to .txt.
% 
% ------------------------------------------------------------------------
% @ Arguments:
% ------------------
% d_set <vector>: [W, min_int, mass_tol, R_min, P_max, r_t, ms_w, ms_p_w,
%                  Mass_w, S2N]
%
% target_path <string>: relative path to target compounds,  
%                           e.g. '../data/Targets_3.xlsx'.
%
% path_low <string>   : relative path to low energy HRMS data dir,
%                           e.g. '../data/High_Energy'
%
% path_high <string>  : relative path to high energy HRMS data dir,
%                           e.g. '../data/High_Energy'
% 
% ------------------------------------------------------------------------
% @ Saved:
% ------------------
% Data saved to "deconv_xxxxxxxxxxxxxx.txt", where "xxx...xx" details the 
% UNIX time when deconvolution spectra is saved.
%
% ------------------------------------------------------------------------
% (c) Samanipour et al. (2018) 

%% FOLLOWING CODE IS TO BE COMPILED 
if (iscell(d_set))
    d_set = cell2mat(d_set);
end

% Get deconvolution_settings
W=d_set(1);                          % Saer label here
min_int=d_set(2);                    % ...
Mass_tol=d_set(3);                   %
R_min=d_set(4);                      %
P_max=d_set(5);                      %
r_t=d_set(6);                        %
ms_w=d_set(7);                       %
ms_p_w=d_set(8);                     %
Mass_w=d_set(9);                     %
S2N=d_set(10);                       %

% NOTE: following 4 executable lines could be reduced to two.
files_array_low = strcat(path_low, filesep, '*.CDF'); 
files_array_high = strcat(path_high, filesep, '*.CDF'); 
% targets of high and low energy batches
hrms_low = ls(files_array_low); 
hrms_high = ls(files_array_high); 

%% THE FOLLOWING CODE IS TO BE COMPILED

% Check if there are files... there should be none.
if ~isempty(dir('../data/deconv/deconv_*.txt'))
    % Move previous file to _superseded doc.
    disp("Warning: Too many deconv_*.txt files");
    movefile('../data/deconv/deconv_*', ...
        '../data/deconv/_superseded/');
    disp("Older files in '~/data/deconv/' have been moved to" + ...
        " '~/data/deconv/_superseded/'.")
end

% commence deconv
% 
% THIS WOULD BE FOR LOOPED IN MATLAB
file_low = strcat(hrms_low(1,:)); 
file_high = strcat(hrms_high(1,:)); 

% IMPORT HRMS_data
[total_intensity,t0,t_end,Mz_values,Mz_intensity,scan_duration] = ...
    netCDF_import (file_low);

[total_intensity2,t0_2,t_end_2,Mz_values_2,Mz_intensity_2, ...
    scan_duration_2] = netCDF_import (file_high);

% put all HRMS data into struct object
HRMS_data.total_intensity = total_intensity;        % Saer label here
HRMS_data.total_intensity2 = total_intensity2;      % Saer label here
HRMS_data.t0 = t0;                                  % Saer label here
HRMS_data.t0_2 = t0_2;                              % Saer label here
HRMS_data.t_end = t_end;                            % ...
HRMS_data.t_end_2 = t_end_2;                        % 
HRMS_data.Mz_values = Mz_values;                    %
HRMS_data.Mz_values_2 = Mz_values_2;                %
HRMS_data.Mz_intensity = Mz_intensity;              %
HRMS_data.Mz_intensity_2 = Mz_intensity_2;          %
HRMS_data.scan_duartion = scan_duration;            %
HRMS_data.scan_duartion_2 = scan_duration_2;        %

disp("Low and high energy LC-HRMS data successfully imported")

%% IMPORT targets list
targets = readtable(target_path);
for target_iterator=1:size(targets.ID,1)

    % GET PARAMS FOR THIS ITERATION 
    target = targets(target_iterator, :);           
    scan_num = round(target.Retention_time);        % retention time
    ions = [round(target.MZ,3), 100];               % ions of???
    target_id = target.ID;                          % arbitrary ID
    target_mz = target.MZ;                          % Mass x Charge
    
    disp("---------Target ID "+target_id+": MZ "+target_mz+" ---------");
    if (isnan(target_id)==1) % ensure there is data
        disp("Target ID "+target_id+" has failed.");
        continue % go to next iterator
    end
    
    % return spectra.
    user_spectra = Frag_extractor_v1(HRMS_data, scan_num, W, min_int, ...
        Mass_tol, R_min, P_max, r_t, ms_w, ions, ms_p_w, Mass_w, S2N);
    
    %% WRITE TO FILE
    
    [~,name_low,~] = fileparts(file_low); % take name of low energy
    [~,name_high,~] = fileparts(file_high); % take name of high energy
    
    if ~isempty(dir('../data/deconv/deconv_*.txt')) % append data
        fileID = fopen(strcat(ls('../data/deconv/deconv_*.txt')),'a');
    else % file does not exist, so write new data file
        
        unix_time = round(posixtime(datetime),2);
        unix_time = num2str(unix_time);
        file_name = sprintf('../data/deconv/deconv_%014s.txt', unix_time);
        
        fileID = fopen(file_name, 'w');
        
        fprintf("line1: name_low_HRMS, name_high_HRMS,Target ID\n");
        fprintf("line2: ms_v1\n");
        fprintf("line3: ms_v2\n");
        fprintf("line4: ms_in1\n");
        fprintf("line5: ms_in2\n");
        
        % set format: "else" statement executes on first iteration
        content_fmt = "low: %-12s; high: %-12s; Target ID: %-5f;" +...
            " Target MZ: %-10f; Target Ret. Time: %-10f\n";
    end
    
    fprintf(fileID, content_fmt, name_low, name_high, target_id, ...
        target_mz, scan_num);
    fprintf(fileID, "%-10f\n", user_spectra.ms_v1);
    fprintf(fileID, "%-74s\n", mat2str(user_spectra.ms_v2));
    fprintf(fileID, "%-10f\n", user_spectra.ms_in1);
    fprintf(fileID, "%-74s\n", mat2str(user_spectra.ms_in2));
    fclose(fileID);

end

end