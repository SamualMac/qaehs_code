function [ spec ] = Frag_extractor_v2(data_c, Scan_num, W, min_int, ...
                                      Mass_tol, R_min, P_max, r_t, ...
                                      ms_w, Ions, ms_p_w)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

if (Scan_num-W>1&&Scan_num+W<size(data_c.Mz_values,1))
    ms_v1=data_c.Mz_values(Scan_num-W:Scan_num+W,:);
    ms_in1=data_c.Mz_intensity(Scan_num-W:Scan_num+W,:);
    ms_v2=data_c.Mz_values_2(Scan_num-W:Scan_num+W,:);
    ms_in2=data_c.Mz_intensity_2(Scan_num-W:Scan_num+W,:);
    
elseif (Scan_num-W<=1&&Scan_num+W<size(data_c.Mz_values,1))
    ms_v1=data_c.Mz_values(1:Scan_num+W,:);
    ms_in1=data_c.Mz_intensity(1:Scan_num+W,:);
    ms_v2=data_c.Mz_values_2(1:Scan_num+W,:);
    ms_in2=data_c.Mz_intensity_2(1:Scan_num+W,:);
    
elseif (Scan_num-W>1&&Scan_num+W>=size(data_c.Mz_values,1))
    ms_v1=data_c.Mz_values(Scan_num-W:size(data_c.Mz_values,1),:);
    ms_in1=data_c.Mz_intensity(Scan_num-W:size(data_c.Mz_values,1),:);
    ms_v2=data_c.Mz_values_2(Scan_num-W:size(data_c.Mz_values_2,1),:);
    ms_in2=data_c.Mz_intensity_2(Scan_num-W:size(data_c.Mz_values_2,1),:);
    
    
end



%%
%main ion extraction
tv1=abs(Ions(1)-ms_v1);
ref_spec=zeros(size(ms_v1,1),1);
for i=1:size(ms_v1,1)
    [V,J]=min(tv1(i,:));
    if (V<=Mass_tol)
        ref_spec(i)=ms_in1(i,J);
    end
    clear V J    
    
end


%Low energy channel
% Low_E=reshape(Ions,[],2)';
% if (size(Low_E,2)<2)
%     MS_v1=Low_E(1,:)';
%     MS_In1=Low_E(2,:)';
% else
%     MS_v1=Low_E(:,1)';
%     MS_In1=Low_E(:,2)';
% end
% 

% plot(ms_v1(W+1,:),ms_in1(W+1,:))
hold on

tv2=(find(ms_in1(W+1,:)>=min_int));
MS_IN1=zeros(1,size(ms_in1,2));

for i=1:size(tv2,2)
    tv3=ms_in1(W+1,tv2);
    tv4=find(ms_in1(W+1,:)==tv3(i),1);
    tv5=abs(ms_v1(W+1,tv4)-ms_v1);
    tv6=zeros(size(ms_v1,1),1);
    for j=1:size(ms_v1,1)
        [V,J]=min(tv5(j,:));
        if (V<=Mass_tol)
            tv6(j)=ms_in1(j,J);
        end
        clear V J
    end
    
    [M_v,M_i]=max(tv6);
    if (abs(M_i-W+1)<=r_t&&M_v>=min_int)
        E_p=W+find(ref_spec(W+1:end)<=0.1*ref_spec(W),1);
        I_p=(W+1)-find(flipud(ref_spec(1:W))<=0.1*ref_spec(W),1);
        
        
         [R,P] = corrcoef(tv6(I_p:E_p),ref_spec(I_p:E_p));
%         plot( ref_spec./max(ref_spec))
%         hold on 
%         plot(Tv6./max(Tv6),'--')
%         hold off
%         legend(num2str(ms_v2(W+1,Tv4)),num2str(i))
%         pause(1)
% 
%            disp(R)
        if (R(2,1)>=R_min&&P(2,1)<=P_max)
           
           MS_IN1(ms_v1(W+1,:)==ms_v1(W+1,tv4))=ms_in1(W+1,ms_v1(W+1,:)==ms_v1(W+1,tv4));
           
        end
        
        
    end
    
    
    
end



 
%%
%High energy channel

   
Tv2=(find(ms_in2(W+1,:)>=min_int));
MS_IN2=zeros(1,size(ms_in2,2));

for i=1:size(Tv2,2)
    Tv3=ms_in2(W+1,Tv2);
    Tv4=find(ms_in2(W+1,:)==Tv3(i),1);
    Tv5=abs(ms_v2(W+1,Tv4)-ms_v2);
    Tv6=zeros(size(ms_v2,1),1);
    for j=1:size(ms_v2,1)
        [V,J]=min(Tv5(j,:));
        if (V<=Mass_tol)
            Tv6(j)=ms_in2(j,J);
        end
        clear V J
    end
    
    [M_v,M_i]=max(Tv6);
    if (abs(M_i-W+1)<=r_t&&M_v>=min_int)
        E_p=W+find(ref_spec(W+1:end)<=0.1*ref_spec(W),1);
        I_p=(W+1)-find(flipud(ref_spec(1:W))<=0.1*ref_spec(W),1);
        
        
         [R,P] = corrcoef(Tv6(I_p:E_p),ref_spec(I_p:E_p));
%         plot( ref_spec./max(ref_spec))
%         hold on 
%         plot(Tv6./max(Tv6),'--')
%         hold off
%         legend(num2str(ms_v2(W+1,Tv4)),num2str(i))
%         pause(1)
% 
%            disp(R)
        if (R(2,1)>=R_min&&P(2,1)<=P_max)
           
           MS_IN2(ms_v2(W+1,:)==ms_v2(W+1,Tv4))=ms_in2(W+1,ms_v2(W+1,:)==ms_v2(W+1,Tv4));
           
        end
        
        
    end
    
    
    
end

% stem(ms_v2(W+1,:),MS_IN2)
% hold on
% stem(ms_v2(W+1,:),ms_in2(W+1,:),'--r')
% hold off
%pause
%%
%Centeroiding the spectra

[ ms_int1 ] = centroiding_MS_data( MS_IN1,min_int,ms_w );

[ ms_int2 ] = centroiding_MS_data( MS_IN2,min_int,ms_w);


%%
%mass alignment

for i=1:size(ms_v1,2)
    
    [tv3,tv4]=min(abs(ms_v1(i)-ms_v2(W+1,:)));
    if (ms_int2(tv4)<=0&&tv4>W&&tv4+W<=size(ms_int2,2))
        tv5=find(ms_int2(tv4-W:tv4+W)>=min_int);
        for k=1:size(tv5,2)
            tv6=tv4-W+tv5(k)-1;
            if (isempty(tv6)==0&&abs(tv6-tv4)<=W&&abs(ms_v2(W+1,tv4)-ms_v2(W+1,tv6))<=ms_p_w)
                ms_int2(tv4)=ms_int2(tv6);
                ms_int2(tv6)=0;
                
            end
            
        end
        
    elseif (ms_int2(tv4)<=0&&tv4>W&&tv4+W>size(ms_int2,2))
        tv5=find(ms_int2(tv4-W:end)>=min_int);
        for k=1:size(tv5,2)
            tv6=tv4-W+tv5(k)-1;
            if (isempty(tv6)==0&&abs(tv6-tv4)<=W&&abs(ms_v2(W+1,tv4)-ms_v2(W+1,tv6))<=ms_p_w)
                ms_int2(tv4)=ms_int2(tv6);
                ms_int2(tv6)=0;
                
            end
            
        end
        
        
    end
    
    clear tv2 tv3 tv4 tv5 tv6
    
end




%%


% plot(ms_v1(W+1,:),MS_IN1)
% hold on
% plot(ms_v2(W+1,:),MS_IN2,'--r')
 %plot(ms_v2(W+1,:),ms_in2(W+1,:),'r')
%  plot(MS_v1,MS_In1,'or')
%  hold on
%  plot(ms_v2(W+1,:),ms_int2,'--k')
%  xlabel('m/z value')
%  ylabel('Intensity')
%  legend('Low Energy channel','High energy channel')
 
 %hold off    
%pause

spec.ms_v1=ms_v1;
spec.ms_v2=ms_v2(W+1,ms_int2>0);
spec.ms_in1=MS_IN1;
spec.ms_in2=ms_int2(ms_int2>0);


disp(spec)





end

