% readSpecData functions to return a single userSpectra data instance 
% resulting from target and HRMS data.
% 
% @ensure: pathToSpec ends in '/'
% @ensure: data has multiple of 5 lines
% 
% @return: userSpectra - struct object with spectra
% @return: meta - name of target-filename pair, inc. mass + id
% @return: eof - true if final block of data, else false

function [userSpectra, meta, eof] = readSpecData(pathToSpec, startLine)

% forwardSlashException
if pathToSpec(end) == '/'
    disp("do not put a '/' at the end of your pathToSpec argument")
    disp("program should fail")
end

% determine file name
specFile = ls(strcat(pathToSpec, filesep, '*.txt')); 
specFile = strcat(specFile(1,:));

% read file
[fid, errorMessage] = fopen(specFile);

% show error message if failed
if (fid == -1)
    disp(errorMessage)
end

% assign data to readSpec
readSpec = textscan(fid,'%s','delimiter','\n');
fclose(fid); % close fid

% incompleteDataException
numLinesTotal = length(readSpec{1});
if mod(numLinesTotal, 5) ~= 0 || numLinesTotal == 0
    disp("WARNING: YOU HAVE ONLY "+numLinesTotal+" LINES!!!!")
    disp("    -YOU NEED NUMBER OF LINES TO BE MULTIPLE OF 5")
    disp("    -Check deconvolution algorithm output")
end

% only read if the start line is a multiple of 5
if mod(startLine, 5) == 1
    
    for lineNumAll = startLine:(startLine+4)
        
        lineInBlock = mod(lineNumAll, 5);
        
        switch lineInBlock
            case 1 
                % target-HRMS_data file pair?
                meta = readSpec{1}{lineNumAll};
            case 2
                userSpectra.ms_v1 = str2num(readSpec{1}{lineNumAll});
            case 3
                userSpectra.ms_v2 = str2num(readSpec{1}{lineNumAll});
            case 4 
                userSpectra.ms_in1 = str2num(readSpec{1}{lineNumAll});
            case 0 
                userSpectra.ms_in2 = str2num(readSpec{1}{lineNumAll});
                
        end
        
    end
    
    
    % determine if final block of data in .txt file
    if startLine + 4 == numLinesTotal
        eof = true;
    else
        eof = false;
    end

    
else % startLine not multiple of 5, therefore fail.
    disp("Failed to read deconv data. Make sure that the 'startLine' "+...
        "argument is a multiple of 5")
end



end



