%%
%Post processing
clear
close all
clc
%%
%The path to the files
path1='D:\Data\MATLAB\Library_search_fun\Decon_spec_1';
path2='D:\Data\MATLAB\Library_search_fun\Reports_1';

%%
Mass_tollerance=0.003;
mz_precision=3;
Weigth_fun=[0.5,0.5,1,1,1,1,1]; %[ion_match1,ion_match2,MZ_e1,MZ_e2,MZ_e3,D_match,R_match]

a=ls(strcat(path1,'\*.mat'));
Results=zeros(size(a,1),4);

for i=1:size(a,1)
    load(strcat(path1,'\',a(i,:)))
    [pathstr,name,ext] = fileparts(char(strcat(path1,'\',a(i,:))));
    ref_spec=final_spec.ref_MS2_v;
    user_spec=final_spec.dcon_ms_v2;
    MassBank=final_spec.dcon_ms_v1;
    ind=1;
    ref_spec_int=final_spec.ref_MS2_int;
    user_spec_int=final_spec.dcon_ms_in2;
    Meas_Mass=final_spec.dcon_ms_v1;
    
    [ S1,ions,Mass_error ] = ion_count( ref_spec,user_spec,MassBank,Meas_Mass,ind,Mass_tollerance );
    [ MatchFactor ] = Match_factor_calc( ref_spec,ref_spec_int,user_spec,user_spec_int,mz_precision,Mass_tollerance );
    Final_score=sum(Weigth_fun.*[S1,MatchFactor]);
    
    if (exist(strcat(path2,'\',name,'.txt'),'file')>=1)
        
        tab=readtable(strcat(path2,'\',name));
        tv1=find(abs(tab.Score-Final_score)<=0.03,1);
        Results(i,1:2)=S1(1:2);
        Results(i,3)=Final_score;
        Results(i,4)=tv1;
        
    else
        Results(i,:)=0;
    end
    
    
    
    
    
end

%%

% detec_res=Results(:,4);
% detec_res=detec_res(detec_res>0);
% histogram(detec_res)
% 
match_fac=Results(:,3);
match_fac=match_fac(match_fac>0);
% histogram(match_fac,15)

% U_ion_match=Results(:,2);
% U_ion_match=U_ion_match(U_ion_match>0);
bin=[4.75,5,5.25,5.5,5.75,6];

%[h]=hist(match_fac,bin);
[h]=hist(match_fac,10);
h=100*h/sum(h);
bar1(:,3)=h;

%%

figure
bar(bar1)

%histogram(U_ion_match,10)
set(gca,'XTickLabel',num2cell(bin))
set(gca,'FontSize',18)
legend('Low noise','Medium noise','High noise')
xlabel('Final score value')
ylabel('Processed spectra (%)')




