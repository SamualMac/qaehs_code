% has nested functions comp_extractor_SSA_v2.m and Lib_search.m
% took isempty check to outside the function, so that if spec is empty
% the code would not break.

function final_table = library_search(path_MB, path_adducts, source, ...
                                      mode, spec)
%         disp(List.MZ(i))
    % get MassBank data
    [ MassBank ] = comp_extractor_SSA_v2(path_MB, source, mode);
    Parent=0;
    ms_tol_w=0.1; % Da
    Mass_tollerance=0.01;
    mz_precision=3;
    Weigth_fun=[1,1,1,1,1,1,1]; %[ion_match1,ion_match2,MZ_e1,MZ_e2,MZ_e3,D_match,R_match]
%         mode='POSITIVE';

    [ final_table ] = Lib_search(Parent, ms_tol_w, MassBank, spec, ...
                                 mz_precision, Mass_tollerance, ...
                                 Weigth_fun, mode, path_adducts);
    disp(final_table)
% writetable(final_table,strcat('D:\Data\MATLAB\Library_search_fun\NMR\',num2str(List.ID(i)),'.txt'))

end