function DeconvLibrarySearch_v1(d_set, target_path, path_low, path_high,...
    path_MB, source, mode, path_adducts, output_ulsa, output_deconv)
% @ Functionality:
% ------------------
% Import HRMS data; execute the deconvolution algorithm (Samanipour et al.,
% 2018); output user spectra data to .txt; output user spectra to object
% that is passed into ULSA; execute ULSA (Samanipour et al., 2018) to 
% attain list of candidate compounds; output ULSA results to .csv.
% 
% NOTE: max_num_files is assumed under intitial limmitting conditions of 
%       this software. That is, there can be only a maximum of 5 files and 
%       50 targets for every test.
% 
% ------------------------------------------------------------------------
% @ Arguments:
% ------------------
% 
% d_set <vector>: [W, min_int, mass_tol, R_min, P_max, r_t, ms_w, ms_p_w,
%                  Mass_w, S2N]
% 
% target_path <string>: relative path to target compounds,  
%                           e.g. '../data/Targets_3.xlsx'.
% 
% path_low <string>   : relative path to low energy HRMS data dir,
%                           e.g. '../data/High_Energy'
% 
% path_high <string>  : relative path to high energy HRMS data dir,
%                           e.g. '../data/High_Energy'
% 
% path_MB <string> :        Relative path to MassBank Library,
%                               e.g.'../data/MassBank_matlab.mat'
% 
% source <string> :         (refer to Samanipour et al. (2018)), e.g. 'ESI'
% 
% mode <string> :           Whether HRMS tested for positive or negative 
%                           ions (refer to Samanipour et al. (2018)), 
%                               e.g.'POSITIVE';
% 
% path_adducts <string> :   Path to excel file listing candidate adducts, 
%                               e.g. '../data/adducts/Pos_adducts.xlsx'
% 
% path_to_spec <string> :   Path to file containing compound MS spectra, 
%                               e.g. '../data/deconv'
% 
% output_ulsa <string> :    Path to location where results are saved to. 
%                           ULSA results saved to "ULSA_xxxxxxxxxxxx.csv", 
%                           where "xxx...x" details the UNIX time when file 
%                           is saved.
% 
% % output_deconv <string> :   absolute path to location where deconv results 
%                           are saved.
%                           
% ------------------------------------------------------------------------
% (c) Samanipour et al. (2018) 
%

%% setup
test_ID = 1; % simple tracker
first_instance = true;

if (iscell(d_set))
    d_set = cell2mat(d_set);
end

% Get deconvolution_settings
W=d_set(1);                          % Saer label here
min_int=d_set(2);                    % ...
Mass_tol=d_set(3);                   % 
R_min=d_set(4);                      %
P_max=d_set(5);                      %
r_t=d_set(6);                        %
ms_w=d_set(7);                       %
ms_p_w=d_set(8);                     %
Mass_w=d_set(9);                     %
S2N=d_set(10);                       %

files_array_low = strcat(path_low, filesep, '*.CDF'); 
files_array_high = strcat(path_high, filesep, '*.CDF'); 
% targets of high and low energy batches
hrms_low = ls(files_array_low); 
hrms_high = ls(files_array_high); 

% Check if there are deconv files... there should be none
if ~isempty(dir(strcat(output_deconv,'/deconv_*')))
    % Move previous file to _superseded doc.
    try
        movefile(strcat(output_deconv, '/deconv_*'), ...
        strcat(output_deconv, '/_superseded/'));
    catch
        mkdir(strcat(output_deconv,'/_superseded'));
        movefile(strcat(output_deconv, '/deconv_*'), ...
        strcat(output_deconv,'/_superseded/'));
    end
    disp("Older files in output_deconv have been moved to" + ...
        " output_deconv + '/_superseded/'.")
end

% check if ULSA_*.csv file requires movement.
if ~isempty(dir(strcat(output_ulsa,'/ULSA_*')))
    % Move previous file to _superseded doc.
    try
    movefile(strcat(output_ulsa,'/ULSA_*'), ...
             strcat(output_ulsa,'/_superseded/'));
    catch
    mkdir(strcat(output_ulsa,'/_superseded'));
    movefile(strcat(output_ulsa,'/ULSA_*'), ...
             strcat(output_ulsa,'/_superseded/'));
    end
    disp("Older files have been moved to" + ...
        " output_ulsa + '/_superseded/'.")
end


file_low = strcat(hrms_low(1,:)); 
file_high = strcat(hrms_high(1,:)); 

% IMPORT HRMS_data
[total_intensity,t0,t_end,Mz_values,Mz_intensity,scan_duration] = ...
    netCDF_import (file_low);

[total_intensity2,t0_2,t_end_2,Mz_values_2,Mz_intensity_2, ...
    scan_duration_2] = netCDF_import (file_high);

% put all HRMS data into struct object
HRMS_data.total_intensity = total_intensity;        % Saer label here
HRMS_data.total_intensity2 = total_intensity2;      % Saer label here
HRMS_data.t0 = t0;                                  % Saer label here
HRMS_data.t0_2 = t0_2;                              % Saer label here
HRMS_data.t_end = t_end;                            % ...
HRMS_data.t_end_2 = t_end_2;                        % 
HRMS_data.Mz_values = Mz_values;                    %
HRMS_data.Mz_values_2 = Mz_values_2;                %
HRMS_data.Mz_intensity = Mz_intensity;              %
HRMS_data.Mz_intensity_2 = Mz_intensity_2;          %
HRMS_data.scan_duartion = scan_duration;            %
HRMS_data.scan_duartion_2 = scan_duration_2;        %

disp("Low and high energy LC-HRMS data successfully imported")

% IMPORT targets list
targets = readtable(target_path);

for target_iterator=1:size(targets.ID,1)
    % get params for iteration
    target = targets(target_iterator, :);           
    retention_time = round(target.Retention_time);        % retention time
    ions = [round(target.MZ,3), 100];               % ions of???
    target_id = target.ID;                          % arbitrary ID
    target_mz = target.MZ;                          % Mass x Charge
    
    disp("---------Target ID "+target_id+": MZ "+target_mz+" ---------");
    if (isnan(target_id)==1) % ensure there is data
        disp("Target ID "+target_id+" has failed.");
        continue % go to next iterator
    end
    %% commence deconv
    % return spectra
    user_spectra = Frag_extractor_v1(HRMS_data, retention_time, W, min_int, ...
        Mass_tol, R_min, P_max, r_t, ms_w, ions, ms_p_w, Mass_w, S2N);
    
    % write user_spectra to file
    
    [~,name_low,~] = fileparts(file_low); % take name of low energy
    [~,name_high,~] = fileparts(file_high); % take name of high energy
    
    if ~isempty(dir(strcat(output_deconv, '/deconv_*.txt'))) % append data
        fileID = fopen(strcat(ls(strcat(output_deconv,'/deconv_*.txt'))),'a');
    else % file does not exist, so write new data file
        
        unix_time = round(posixtime(datetime),2);
        unix_time = num2str(unix_time);
        file_name = sprintf(strcat(output_deconv, '/deconv_%014s.txt'), ...
            unix_time);
        
        fileID = fopen(file_name, 'w');
        
        fprintf("line1: name_low_HRMS, name_high_HRMS,Target ID\n");
        fprintf("line2: ms_v1\n");
        fprintf("line3: ms_v2\n");
        fprintf("line4: ms_in1\n");
        fprintf("line5: ms_in2\n");
        
        % set format: "else" statement executes on first iteration
        content_fmt = "low: %-12s; high: %-12s; Target ID: %-5f;" +...
            " Target MZ: %-10f; Target Ret. Time: %-10f\n";
    end
    
    fprintf(fileID, content_fmt, name_low, name_high, target_id, ...
        target_mz, retention_time);
    fprintf(fileID, "%-10f\n", user_spectra.ms_v1);
    fprintf(fileID, "%-74s\n", mat2str(user_spectra.ms_v2));
    fprintf(fileID, "%-10f\n", user_spectra.ms_in1);
    fprintf(fileID, "%-74s\n", mat2str(user_spectra.ms_in2));
    fclose(fileID);
    %% run library search if there are ms_v2 contents
    
    if ~isempty(user_spectra.ms_v2)
        disp("Comencing the library search algorithm")
        [ s_ULSA ] = library_search(path_MB, path_adducts, source, ...
            mode, user_spectra); 
    else
        disp("No mass spec. partner, i.e. no MS2 data.")
    end
    
%     % if first iteration instantiate table
%     if test_ID == 1
%         final_ULSA_out  = cell2table(cell(0,14), 'VariableNames', ...
%             {'test_ID', 'low_file', 'high_file', 'target_ID', 'nr', ...
%             'Name', 'Ref_ion_matched', 'User_ion_matched', ...
%             'Parent_ion_error', 'Average_frag_error', 'Std_frag_error', ...
%             'Direct_match', 'Reverse_match', 'Score'});
%     end
    if exist('s_ULSA')
        [n_row_add, ~] = size(s_ULSA);
        disp("Positive target match in library. Saving results.")
    else
        n_row_add = 0;
        disp("Negative target match in library; possibly not registered.")
    end
    
    % write to ULSA_out
    if n_row_add ~= 0
        
        % instantiate empty table on first iteration
        if first_instance 
            ULSA_out = cell2table(cell(0, 16), ...
                'VariableNames', ...
                {'test_ID', 'low_file', 'high_file', 'target_id', ...
                'target_mz', 'retention_time', 'nr', 'Name', ...
                'Ref_ion_matched', 'User_ion_matched', ...
                'Parent_ion_error', 'Average_frag_error', ...
                'Std_frag_error', 'Direct_match', 'Reverse_match', ...
                'Score'});
        end
        
        % add s_ULSA output
        for toAdd_index = 1:n_row_add
            new_row = {...
                test_ID, name_low, name_high, target_id, target_mz, ...
                retention_time, ...
                s_ULSA.nr(toAdd_index), ...
                s_ULSA.Name(toAdd_index), ...
                s_ULSA.Ref_ion_matched(toAdd_index), ...
                s_ULSA.User_ion_matched(toAdd_index), ...
                s_ULSA.Parent_ion_error(toAdd_index), ...
                s_ULSA.Average_frag_error(toAdd_index), ...
                s_ULSA.Std_frag_error(toAdd_index), ...
                s_ULSA.Direct_match(toAdd_index), ...
                s_ULSA.Reverse_match(toAdd_index), ...
                s_ULSA.Score(toAdd_index)...
            };
            % concat row to end of table
            ULSA_out = [ULSA_out; new_row];
        end
        
        first_instance = false;
    end
    
%     
%     % append data
%     [nrowToAdd, ~] = size(s_ULSA);
%     [nrowFinalPrior, ~] = size(final_ULSA_out);
%     for toAdd_index = 1:nrowToAdd
%         newRow = {...
%             test_ID, name_low, name_high, target_id, ...
%             s_ULSA.nr(toAdd_index), ...
%             s_ULSA.Name(toAdd_index), ...
%             s_ULSA.Ref_ion_matched(toAdd_index), ...
%             s_ULSA.User_ion_matched(toAdd_index), ...
%             s_ULSA.Parent_ion_error(toAdd_index), ...
%             s_ULSA.Average_frag_error(toAdd_index), ...
%             s_ULSA.Std_frag_error(toAdd_index), ...
%             s_ULSA.Direct_match(toAdd_index), ...
%             s_ULSA.Reverse_match(toAdd_index), ...
%             s_ULSA.Score(toAdd_index)...
%             };
%         
%         final_ULSA_out = [final_ULSA_out; newRow];
%     end
%     
%     test_ID = test_ID + 1;
%    

% save to hard drive
if target_iterator == size(targets.ID,1) % && j == numFilePairs
    unix_time = round(posixtime(datetime),2);
    unix_time = num2str(unix_time);
    file_name = sprintf(strcat(output_ulsa,'/ULSA_%014s.csv'), ...
        unix_time);
    writetable(ULSA_out, file_name)
    disp("Saved results to ULSA_00"+num2str(unix_time)+".csv.")
%         writetable(final_ULSA_out,'../data/final_ULSA_out.txt', ...
%             'Delimiter',',')
%         writetable(final_ULSA_out,'../data/final_ULSA_out.csv')
end
    
end

