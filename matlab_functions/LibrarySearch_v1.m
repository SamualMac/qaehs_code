function LibrarySearch_v1(path_MB, source, mode, path_adducts, ...
    path_to_spec, output_ulsa)
% ------------------------------------------------------------------------
% @ Functionality:
% ------------------
% Read spec data from deconv_*.txt output from ImportDeconv.m; execute ULSA 
% (Samanipour et al., 2018) attain list of candidate compounds; output ULSA
% results to .csv.
% 
% NOTE [1]: It is important that the output_ulsa location has a _superseded
%           repository. This is not yet implemented in the code.
%
% NOTE [2]: max_num_files is admin set by the intitial limmitting 
%           conditions of this software. Gauranteed to 14/04/18, there can 
%           be only a maximum of 5 files and 50 targets for every test. 
%           This allows fair sharing of computational resources between 
%           scientists.
% 
% ------------------------------------------------------------------------
% @ Arguments:
% ------------------
% path_MB <string> :        Relative path to MassBank Library,
%                               e.g.'../data/MassBank_matlab.mat'
%
% source <string> :         (refer to Samanipour et al. (2018)), e.g. 'ESI'
%
% mode <string> :           Whether HRMS tested for positive or negative 
%                           ions (refer to Samanipour et al. (2018)), 
%                               e.g.'POSITIVE';
%
% path_adducts <string> :   Path to excel file listing candidate adducts, 
%                               e.g. '../data/adducts/Pos_adducts.xlsx'
%
% path_to_spec <string> :   Path to file containing compound MS spectra, 
%                               e.g. '../data/deconv'
%
% output_ulsa <string> :    Path to location where results are saved to. 
%                           ULSA results saved to "ULSA_xxxxxxxxxxxx.csv", 
%                           where "xxx...x" details the UNIX time when file 
%                           is saved.
% 
% ------------------------------------------------------------------------
% (c) Samanipour et al. (2018) 

%% TO COMPILE

% initiate variables
test_ID = 0; % test iteration counter
% numFilePairs = 1;
currentLine = 1; % line position of latest write
eof = false; % end of file of ????
first_instance = true; % first iteration flag

% check if file requires movement.
if ~isempty(dir(strcat(output_ulsa,'/ULSA_*.csv')))
    % Move previous file to _superseded doc.
    try
    movefile(strcat(output_ulsa,'/ULSA_*'), ...
             strcat(output_ulsa,'/_superseded/'));
    catch
    mkdir(strcat(output_ulsa,'/_superseded'));
    movefile(strcat(output_ulsa,'/ULSA_*'), ...
             strcat(output_ulsa,'/_superseded/'));
    end
    disp("Older files have been moved to" + ...
        " output_ulsa + '/_superseded/'.")
end

while not(eof)
    % todo - suspect.
    clear s_ULSA % ensure we don't duplicate results from last iteration
    
    test_ID = test_ID + 1;
    disp("---------------------------------------------------------------")
    disp("Loading batch...")
    
    % read single batch of spectra data
    [user_spectra, meta, eof] = readSpecData(path_to_spec, currentLine);
    
    % extract meta data
    meta_split = strsplit(meta, ';');
    name_low = meta_split{1}(6:end); % "6:end" exclude labels within string
    name_high = meta_split{2}(8:end);
    target_id = meta_split{3}(13:end);
    target_mz = meta_split{4}(13:end);
    retention_time = meta_split{5}(20:end);
    
    disp("Target {ID: "+target_id+", MZ: "+target_mz+...
        ", Ret. Time: "+retention_time)
    
    currentLine = currentLine + 5; % for next iteration
        
    % if there are ms2 contents, extract single instance s_ULSA
    if ~isempty(user_spectra.ms_v2)
        [ s_ULSA ] = library_search(path_MB, path_adducts, source, ...
                                 mode, user_spectra);
    else
        disp("No mass spec. partner, i.e. no MS2 data")
    end
    
    if exist('s_ULSA')
        [n_row_add, ~] = size(s_ULSA);
        disp("Positive target match in library. Saving results.")
    else
        n_row_add = 0;
        disp("Negative target match in library; possibly not registered.")
    end

    % write to ULSA_out
    if n_row_add ~= 0

        % instantiate empty table on first iteration
        if first_instance
            ULSA_out = cell2table(cell(0, 16), ...
                'VariableNames', ...
                {'test_ID', 'low_file', 'high_file', 'target_id', ...
                'target_mz', 'retention_time', 'nr', 'Name', ...
                'Ref_ion_matched', 'User_ion_matched', ...
                'Parent_ion_error', 'Average_frag_error', ...
                'Std_frag_error', 'Direct_match', 'Reverse_match', ...
                'Score'});
        end
        
        % add s_ULSA ouput
        for toAdd_index = 1:n_row_add
            new_row = {...
                test_ID, name_low, name_high, target_id, target_mz, ...
                retention_time, ...
                s_ULSA.nr(toAdd_index), ...
                s_ULSA.Name(toAdd_index), ...
                s_ULSA.Ref_ion_matched(toAdd_index), ...
                s_ULSA.User_ion_matched(toAdd_index), ...
                s_ULSA.Parent_ion_error(toAdd_index), ...
                s_ULSA.Average_frag_error(toAdd_index), ...
                s_ULSA.Std_frag_error(toAdd_index), ...
                s_ULSA.Direct_match(toAdd_index), ...
                s_ULSA.Reverse_match(toAdd_index), ...
                s_ULSA.Score(toAdd_index)...
            };
            % concat row to end of table
            ULSA_out = [ULSA_out; new_row];
            
        end

        first_instance = false;
        
    end
    
end

if eof % save to hard drive
%     ULSA_out = sortrows(ULSA_out, [5 17]);
    unix_time = round(posixtime(datetime),2);
    unix_time = num2str(unix_time);
    file_name = sprintf(strcat(output_ulsa,'/ULSA_%014s.csv'), ...
        unix_time);
    writetable(ULSA_out, file_name)
    disp("Saved results to ULSA_00"+num2str(unix_time)+".csv.")
end 

end