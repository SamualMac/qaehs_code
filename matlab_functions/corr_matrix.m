function [R] = corr_matrix(x_,y_)
%Divide covariance matrix by std(x_)std(y_) to normalise, thus receiving
%pearson correlation matrix.
    Cov_ = cov(x_,y_);
    R = Cov_/(std(x_)*std(y_));
    
    
end

